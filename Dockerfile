FROM golang:alpine

RUN apk add --no-cache bash mysql mysql-client

ENV DB_USER ""
ENV DB_PASS ""
ENV DB_NAME ""
ENV DB_HOST ""
ENV ALL_DATABASES 0

WORKDIR /go/src/app

COPY . .
RUN go build

CMD ["sh", "-c", "./mysql-backup-service run --db_host=${DB_HOST} --db_user=${DB_USER} --db_pass=${DB_PASS} --db_name=${DB_NAME} --all_databases=${ALL_DATABASES}"]
