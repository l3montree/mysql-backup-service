package backup

import (
	"bufio"
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

type MySQLCredentials struct {
	dbPass string
	dbUser string
	dbName string
	dbHost string
}


func Do(ctx *cli.Context) error {
	var cred = MySQLCredentials{
		dbPass: ctx.String("db_pass"),
		dbUser: ctx.String("db_user"),
		dbName: ctx.String("db_name"),
		dbHost: ctx.String("db_host"),
	}

	pathToScript, _ := filepath.Abs("dump.sh")

	cmd := exec.Command("/bin/bash", pathToScript)
	cmd.Env = os.Environ()

	cmd.Env = append(
		cmd.Env,
		fmt.Sprintf("DB_USER=%s", cred.dbUser),
		fmt.Sprintf("DB_PASS=%s", cred.dbPass),
		fmt.Sprintf("DB_NAME=%s", cred.dbName),
		fmt.Sprintf("DB_HOST=%s", cred.dbHost),
		fmt.Sprintf("ALL_DATABASES=%t", ctx.Bool("all_databases")),
	)
	stderr, _ := cmd.StderrPipe()
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(stderr)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	return cmd.Wait()
}
