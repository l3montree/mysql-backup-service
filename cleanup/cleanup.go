package cleanup

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"io/ioutil"
	"log"
	"math"
	"os"
	"time"
)

var yearWeekStringArr []string

func shouldDeleteFolder(year int, week int) bool {
	toCompare := fmt.Sprintf("%d%d", year, week)
	for _, yearWeekString := range yearWeekStringArr {
		if toCompare == yearWeekString {
			return true
		}
	}
	yearWeekStringArr = append(yearWeekStringArr, toCompare)
	return false
}

func Do(context *cli.Context) error {
	path := "/mysqldump"
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}
	today := time.Now()
	for _, file := range files {
		if file.IsDir() {
			// we are only looking for directories
			fmt.Println(file.Name())
			// parse the time
			if date, err := time.Parse("2006_01_02", file.Name()); err == nil {
				year, week := date.ISOWeek()
				// lets check if this belongs to this week
				// we keep all backups created this week.
				hours := math.Abs(date.Sub(today).Hours())
				if hours < (24 * 7) {
					continue
				} else if hours > 24 * 7 * 4 * 3 {
					// will delete all files which were created 3 months ago.
					err := os.RemoveAll(fmt.Sprintf("%s/%s", path, file.Name()))
					if err != nil {
						log.Fatal(err)
					}
					continue
				}
				if shouldDeleteFolder(year, week) {
					err := os.RemoveAll(fmt.Sprintf("%s/%s", path, file.Name()))
					if err != nil {
						log.Fatal(err)
					}
				}
			}
		}
	}
	return nil
}
