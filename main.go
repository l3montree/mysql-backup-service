package main

import (
	"fmt"
	"github.com/urfave/cli/v2"
	"log"
	"mysql-backup-service/backup"
	"mysql-backup-service/cleanup"
	"os"
)

func main() {
	var mysqlCredFlags = []cli.Flag {
		&cli.StringFlag{
			Name: "db_host",
			Usage: "The database host",
			Required: true,
		},
		&cli.BoolFlag{
			Name: "all_databases",
			Usage: "If set to true, the whole server will be backup-ed",
		},
		&cli.StringFlag{
			Name: "db_name",
			Usage: "Provide a name of the database to backup. If all_databases is defined, the db_name will be ignored",
		},
		&cli.StringFlag{
			Name: "db_user",
			Usage: "The user - needs the correct permissions to access the databases",
			Required: true,
		},
		&cli.StringFlag{
			Name: "db_pass",
			Usage: "The user password",
			Required: true,
		},
	}

	app := &cli.App{
		Commands: []*cli.Command{
			{
				Flags: mysqlCredFlags,
				Aliases: []string{"r"},
				Name: "run",
				Usage: "Run the backup followed by a cleanup to remove old database backups",
				Action: func(context *cli.Context) error {
					fmt.Println("Running Backups")
					if err := backup.Do(context); err != nil {
						log.Fatal(err)
						return err
					}
					return cleanup.Do(context)
				},
			},
			{
				Aliases: []string{"c"},
				Name:    "cleanup",
				Usage:   "Clean up old database backups (currently only a default strategy is supported)",
				Action:  cleanup.Do,
			},
			{
				Flags: mysqlCredFlags,
				Name: "backup",
				Aliases: []string{"b"},
				Usage: "create a backup of the database or databases",
				Action: backup.Do,
			},
		},
	}
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
